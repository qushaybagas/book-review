

package ta.perpus.coba1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
 
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

 
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class F5Activity extends DashboardActivity 
{
public TextView txtTitle,txtAuthor,txtISBN,txtPublisher,txtPublishDate,txtNumberPage,txtDescription;
private ImageView pictBook;
private ImageButton btnComment;
private String url,url2,DATA2;
public String ISBN;

protected void onCreate(Bundle savedInstanceState) 
{
    super.onCreate(savedInstanceState);
    setContentView (R.layout.activity_f5);
    setTitleFromActivityLabel (R.id.title_text);
    
    url = getString(R.string.url_server)+"data_buku.php?isbn=";
    url2 = getString(R.string.url_server)+"isbntokoleksi.php?isbn=";
    
    Bundle bundle = this.getIntent().getExtras();
    ISBN = bundle.getString("param1");
    url+=ISBN;
    url2+=ISBN;
    
    txtTitle = (TextView)findViewById(R.id.title);
    txtAuthor = (TextView)findViewById(R.id.author);
    txtISBN = (TextView)findViewById(R.id.isbn);
    txtPublisher = (TextView)findViewById(R.id.publiher);
    txtPublishDate = (TextView)findViewById(R.id.publishdate);
    txtNumberPage = (TextView)findViewById(R.id.numberpage);
    pictBook = (ImageView)findViewById(R.id.pict);
    txtDescription = (TextView)findViewById(R.id.description);
    btnComment=(ImageButton)findViewById(R.id.btnComment);
    btnComment.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			getMessagePHP(url2);
			Bundle bundle2 = new Bundle();
        	bundle2.putString("koleksi", DATA2);
        	Intent newIntent2 = new Intent(getApplicationContext(), CommentActivity.class);
        	newIntent2.putExtras(bundle2);
        	startActivityForResult(newIntent2, 0);
			
		}
	});
    
    
    getMessagePHP(url);
    txtISBN.setText(ISBN);
    txtPublishDate.setText("2010");
    txtNumberPage.setText("543");
    txtDescription.setText("buku android buku android buku android buku android buku android");

    Bitmap PictBook=BitmapFactory.decodeResource(getResources(), R.drawable.book);
    pictBook.setImageBitmap(PictBook);
    
    final RatingBar ratingBar_default = (RatingBar)findViewById(R.id.ratingbar_default);

    ratingBar_default.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){

    	   @Override
    	   public void onRatingChanged(RatingBar ratingBar, float rating,
    	     boolean fromUser) {
    	    Toast.makeText(F5Activity.this, "rating:"+String.valueOf(rating),Toast.LENGTH_LONG).show();
    	   }});
}

private String getMessagePHP(String alamat) 
{
	int BUFFER_SIZE = 10;
	InputStream in = null;
	int charRead;
    String str = "";
    String hasil;
	
	try {
        in = OpenHttpConnection(alamat);
    } catch (IOException e1) {
        e1.printStackTrace();
        return "";
    }
    InputStreamReader isr = new InputStreamReader(in);
    
    char[] inputBuffer = new char[BUFFER_SIZE];
    try {
    	while ((charRead = isr.read(inputBuffer))>0) {
    		//--convert chars to string
    		String readString = String.copyValueOf(inputBuffer, 0, charRead);
    		str += readString;
    		inputBuffer = new char [BUFFER_SIZE];
    	}
    	hasil=str.toString();
    	if  (alamat.equals(url)){
	    	String[] DATA=hasil.split("~");
	    	txtTitle.setText(DATA[0]);
	    	txtAuthor.setText(DATA[1]);
	    	txtPublisher.setText(DATA[2]);
	    }else if (alamat.equals(url2)) {
			DATA2=hasil;
		}
    	in.close();
    }
    catch (IOException ex) {
    	ex.printStackTrace();
    	return "";
    }
    return str;
}
private InputStream OpenHttpConnection(String urlString) throws IOException
{
    InputStream in = null;
    int response = -1;
           
    URL url = new URL(urlString); 
    URLConnection conn = url.openConnection();
	//Toast.makeText(getBaseContext(), url.toString(), Toast.LENGTH_SHORT).show();
	  
    if (!(conn instanceof HttpURLConnection))                     
        throw new IOException("Not an HTTP connection");
    
    try{
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false);
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setRequestMethod("GET");
        httpConn.connect(); 

        response = httpConn.getResponseCode();                 
        if (response == HttpURLConnection.HTTP_OK) {
            in = httpConn.getInputStream();                                 
        }                     
    }
    catch (Exception ex)
    {
        throw new IOException("Error connecting");            
    }
    return in;     
}
 
} // end class
