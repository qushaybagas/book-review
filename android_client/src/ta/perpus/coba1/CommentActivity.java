package ta.perpus.coba1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts.Data;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class CommentActivity extends DashboardActivity {
	private String url;

    private static String[] DATA = {""};
    private static String[] DATA2 = {""};
	public String id_koleksi,pesan,jml;
	public int jumlah;
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_comment);
	        setTitleFromActivityLabel (R.id.title_text);
	        url = getString(R.string.url_server)+"comment.php?id=";
	        Bundle bundle = this.getIntent().getExtras();
	        id_koleksi = bundle.getString("koleksi");
	        url+=id_koleksi;
	        getMessagePHP(url);
	        
	        
	        if (jumlah==1){
	        	Toast.makeText(CommentActivity.this, "Tidak ada komentar" , Toast.LENGTH_LONG).show();
	        }else{
	        	ArrayList<SearchResults> searchResults = GetSearchResults();
		        final ListView lv1 = (ListView) findViewById(R.id.ListView01);
		        lv1.setAdapter(new BaseAdapterComment(this, searchResults));
		       
		        lv1.setOnItemClickListener(new OnItemClickListener() {
		        @Override
				public void onItemClick(AdapterView<?> a, View v, int position,long id) {
					Object o = lv1.getItemAtPosition(position);
			          SearchResults fullObject = (SearchResults)o;
			          AlertDialog.Builder alertbox = new AlertDialog.Builder(CommentActivity.this);
			  		alertbox.setMessage(fullObject.getPesan());
			  		alertbox.setIcon(R.drawable.iconcomment2);
			  		alertbox.setTitle(fullObject.getName());
			  		alertbox.setPositiveButton("Close", new DialogInterface.OnClickListener() {
			              public void onClick(DialogInterface arg0, int arg1) {
			              	
			              }
			          });
			          alertbox.show();	
			          
			          //Toast.makeText(CommentActivity.this, "You have chosen: " + " " + fullObject.getName(), Toast.LENGTH_LONG).show();
			         
				} 
		        });
	        }
	    }
	    private ArrayList<SearchResults> GetSearchResults(){
        	ArrayList<SearchResults> results = new ArrayList<SearchResults>();
        	SearchResults sr1 ;
        	for (int i=0;i<=DATA.length-1;i++){
		    	 DATA2=DATA[i].split("~");
			     sr1 = new SearchResults();
			     sr1.setName(DATA2[0]);
			     sr1.setTime(DATA2[1]);
			     sr1.setPesan(DATA2[2]);
			     results.add(sr1);
        	}
        	return results;
	    }
	    private String getMessagePHP(String alamat) 
	    {
	    	int BUFFER_SIZE = 10;
	    	InputStream in = null;
	    	int charRead;
	        String str = "";
	        String hasil;
	    	
	    	try {
	            in = OpenHttpConnection(alamat);
	        } catch (IOException e1) {
	            // TODO Auto-generated catch block
	            e1.printStackTrace();
	            return "";
	        }
	        InputStreamReader isr = new InputStreamReader(in);
	        
	        char[] inputBuffer = new char[BUFFER_SIZE];
	        try {
	        	while ((charRead = isr.read(inputBuffer))>0) {
	        		//--convert chars to string
	        		String readString = String.copyValueOf(inputBuffer, 0, charRead);
	        		str += readString;
	        		inputBuffer = new char [BUFFER_SIZE];
	        	}
	        	hasil=str.toString();
	        	if (hasil.equals("0")){
	        		jumlah=1;
	        	}else{
	        		DATA=hasil.split("#");
	        	}
	        	in.close();
	        }
	        catch (IOException ex) {
	        	ex.printStackTrace();
	        	return "";
	        }
	        return str;
	    }
		private InputStream OpenHttpConnection(String urlString) throws IOException
	    {
	        InputStream in = null;
	        int response = -1;
	               
	        URL url = new URL(urlString); 
	        URLConnection conn = url.openConnection();
	    	//Toast.makeText(getBaseContext(), url.toString(), Toast.LENGTH_SHORT).show();
	    	  
	        if (!(conn instanceof HttpURLConnection))                     
	            throw new IOException("Not an HTTP connection");
	        
	        try{
	            HttpURLConnection httpConn = (HttpURLConnection) conn;
	            httpConn.setAllowUserInteraction(false);
	            httpConn.setInstanceFollowRedirects(true);
	            httpConn.setRequestMethod("GET");
	            httpConn.connect(); 

	            response = httpConn.getResponseCode();                 
	            if (response == HttpURLConnection.HTTP_OK) {
	                in = httpConn.getInputStream();                                 
	            }                     
	        }
	        catch (Exception ex)
	        {
	            throw new IOException("Error connecting");            
	        }
	        return in;     
	    }
	
}