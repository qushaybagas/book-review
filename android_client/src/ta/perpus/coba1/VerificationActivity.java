
package ta.perpus.coba1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class VerificationActivity extends DashboardActivity
{
	private String url,url_ori;
    //private String url = "http://10.0.2.2/perpus/registrasi.php";
    private Button btnVer;
    private EditText edtVerisikasi;
    private String sms = "";
	private String[] pesan ={};

protected void onCreate(Bundle savedInstanceState) 
{
    super.onCreate(savedInstanceState);
    setContentView (R.layout.activity_verification);
    setTitleFromActivityLabel (R.id.title_text);
    
 // Read sms message
    Uri uriSMSURI = Uri.parse("content://sms/inbox");
    Cursor cur = getContentResolver().query(uriSMSURI, null, null, null,null);
    String sms = "";
    while (cur.moveToNext()) {
  	  if (cur.getString(2).equals("+6281327219704")){
        sms +=  cur.getString(11)+":";
        
  	  }
    }
    pesan=sms.split(":");
    
    url_ori=getString(R.string.url_server)+"verifikasi.php";
    url=url_ori;
    edtVerisikasi=(EditText)findViewById(R.id.verifikasi);
    edtVerisikasi.setText(pesan[0]);
    
    btnVer =(Button)findViewById(R.id.btnverifikasi);
    btnVer.setOnClickListener(new Button.OnClickListener(){
        public void onClick(View v){
        	
        	url+="?ver="+edtVerisikasi.getText().toString().trim();
        	//Toast.makeText(getApplicationContext(),url.toString(), Toast.LENGTH_LONG).show();
	        getMessagePHP(url);
	        url=url_ori;
	        	
        }
    });
    
    
}
private String getMessagePHP(String alamat) 
{
	int BUFFER_SIZE = 10;
	InputStream in = null;
	int charRead;
    String str = "";
    String hasil;
	
	try {
        in = OpenHttpConnection(alamat);
    } catch (IOException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
        return "";
    }
    InputStreamReader isr = new InputStreamReader(in);
    
    char[] inputBuffer = new char[BUFFER_SIZE];
    try {
    	while ((charRead = isr.read(inputBuffer))>0) {
    		//--convert chars to string
    		String readString = String.copyValueOf(inputBuffer, 0, charRead);
    		str += readString;
    		inputBuffer = new char [BUFFER_SIZE];
    	}
    	hasil=str.toString();
    	//Toast.makeText(getApplicationContext(),hasil, Toast.LENGTH_LONG).show();
    	if (hasil.equals("1")){
        	startActivity (new Intent(getApplicationContext(), loginActivity.class));
        	Toast.makeText(getApplicationContext(),"Verifikasi Berhasil!", Toast.LENGTH_LONG).show();}
    	else {Toast.makeText(getApplicationContext(),"Verifikasi gagal!", Toast.LENGTH_LONG).show();
		}
    	in.close();
    }
    catch (IOException ex) {
    	ex.printStackTrace();
    	return "";
    }
    return str;
}
private InputStream OpenHttpConnection(String urlString) throws IOException
{
    InputStream in = null;
    int response = -1;
           
    URL url = new URL(urlString); 
    URLConnection conn = url.openConnection();
	//Toast.makeText(getBaseContext(), url.toString(), Toast.LENGTH_SHORT).show();
	  
    if (!(conn instanceof HttpURLConnection))                     
        throw new IOException("Not an HTTP connection");
    
    try{
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false);
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setRequestMethod("GET");
        httpConn.connect(); 

        response = httpConn.getResponseCode();                 
        if (response == HttpURLConnection.HTTP_OK) {
            in = httpConn.getInputStream();                                 
        }                     
    }
    catch (Exception ex)
    {
        throw new IOException("Error connecting");            
    }
    return in;     
}
    
} // end class
