package ta.perpus.coba1;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseAdapterProfile extends BaseAdapter {
	 private static ArrayList<SearchResults> searchArrayList;
	 
	 private LayoutInflater mInflater;

	 public BaseAdapterProfile(Context context, ArrayList<SearchResults> results) {
	  searchArrayList = results;
	  mInflater = LayoutInflater.from(context);
	 }

	 public int getCount() {
	  return searchArrayList.size();
	 }

	 public Object getItem(int position) {
	  return searchArrayList.get(position);
	 }

	 public long getItemId(int position) {
	  return position;
	 }

	 public View getView(int position, View convertView, ViewGroup parent) {
	  ViewHolder holder;
	  if (convertView == null) {
	   convertView = mInflater.inflate(R.layout.row_view_profile, null);
	   holder = new ViewHolder();
	   holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
	   holder.txtBookCode = (TextView) convertView.findViewById(R.id.bookcode);
	   holder.txtIsbn = (TextView) convertView.findViewById(R.id.isbn);
	   holder.txtLoanDate = (TextView) convertView.findViewById(R.id.loandate);
	   holder.txtLimitDate = (TextView) convertView.findViewById(R.id.limitdate);
	   holder.txtlate = (TextView) convertView.findViewById(R.id.late);
	   holder.pictBook = (ImageView) convertView.findViewById(R.id.pict);

	   convertView.setTag(holder);
	  } else {
	   holder = (ViewHolder) convertView.getTag();
	  }
	  
	  holder.txtTitle.setText(searchArrayList.get(position).getTitle());
	  holder.txtBookCode.setText(searchArrayList.get(position).getBookCode());
	  holder.txtIsbn.setText(searchArrayList.get(position).getIsbn());
	  holder.txtLoanDate.setText(searchArrayList.get(position).getLoanDate());
	  holder.txtLimitDate.setText(searchArrayList.get(position).getLimitDate());
	  holder.txtlate.setText(searchArrayList.get(position).getLate());
	  holder.pictBook.setImageBitmap(searchArrayList.get(position).getPictBook());
	  return convertView;
	 }

	 static class ViewHolder {
	  TextView txtTitle,txtBookCode,txtIsbn,txtLoanDate,txtLimitDate,txtlate;
	  ImageView pictBook;
	 }
	}