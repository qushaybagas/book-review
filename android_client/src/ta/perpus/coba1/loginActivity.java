
package ta.perpus.coba1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class loginActivity extends Activity 
{
    private String url,url_ori;
    private Button btnLogin;
    private TextView btnReg;
    private EditText edtUser,edtPass;
    private static final String APP_SHARED_PREFS = "ta.perpus.coba1.user";

	protected void onCreate(Bundle savedInstanceState){
	    super.onCreate(savedInstanceState);
	    setContentView (R.layout.activity_login);
	    url_ori=getString(R.string.url_server)+"login.php";
	    url=url_ori;
	    edtUser=(EditText)findViewById(R.id.user);
	    edtUser.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				edtUser.setText("");
			}
		});
	    edtPass=(EditText)findViewById(R.id.password);
	    edtPass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				edtPass.setText("");
			}
		});
	    btnLogin =(Button)findViewById(R.id.login);
	    btnLogin.setOnClickListener(new Button.OnClickListener(){
	        public void onClick(View v){
	        	url+="?user="+edtUser.getText().toString()+"&pass="+edtPass.getText().toString().trim();
	        	getMessagePHP(url);
	        	//Toast.makeText(getApplicationContext(),url.toString(), Toast.LENGTH_LONG).show();
	        	
	        	url=url_ori;
	        }
	    });
	    btnReg=(TextView)findViewById(R.id.reg);
	    btnReg.setText(Html.fromHtml("<u>Sign Up</u> "));
	    btnReg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity (new Intent(getApplicationContext(), RegActivity.class));
				
			}
		});
	}
	private String getMessagePHP(String alamat) 
	{
		int BUFFER_SIZE = 10;
		InputStream in = null;
		int charRead;
	    String str = "";
	    String hasil;
		
		try {
	        in = OpenHttpConnection(alamat);
	    } catch (IOException e1) {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
	        return "";
	    }
	    InputStreamReader isr = new InputStreamReader(in);
	    
	    char[] inputBuffer = new char[BUFFER_SIZE];
	    try {
	    	while ((charRead = isr.read(inputBuffer))>0) {
	    		//--convert chars to string
	    		String readString = String.copyValueOf(inputBuffer, 0, charRead);
	    		str += readString;
	    		inputBuffer = new char [BUFFER_SIZE];
	    	}
	    	hasil=str.toString();
	    	//Toast.makeText(getApplicationContext(),hasil, Toast.LENGTH_LONG).show();
	    	if (hasil.equals("1")){
	    		SavePreferences("USER", edtUser.getText().toString());
	    		Bundle bundle2 = new Bundle();
	        	bundle2.putString("user", edtUser.getText().toString());
	        	Intent newIntent2 = new Intent(getApplicationContext(), PerpusActivity.class);
	        	newIntent2.putExtras(bundle2);
	        	startActivityForResult(newIntent2, 0);}
	    	else if (hasil.equals("2")){
	    		Toast.makeText(getApplicationContext(),"User belum terverifikasi!", Toast.LENGTH_LONG).show();
	        	startActivity (new Intent(getApplicationContext(), VerificationActivity.class));}
	    	else {Toast.makeText(getApplicationContext(),"Login Gagal", Toast.LENGTH_LONG).show();
			}
	    	in.close();
	    }
	    catch (IOException ex) {
	    	ex.printStackTrace();
	    	return "";
	    }
	    return str;
	}
	private InputStream OpenHttpConnection(String urlString) throws IOException
{
    InputStream in = null;
    int response = -1;
           
    URL url = new URL(urlString); 
    URLConnection conn = url.openConnection();
	//Toast.makeText(getBaseContext(), url.toString(), Toast.LENGTH_SHORT).show();
	  
    if (!(conn instanceof HttpURLConnection))                     
        throw new IOException("Not an HTTP connection");
    
    try{
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false);
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setRequestMethod("GET");
        httpConn.connect(); 

        response = httpConn.getResponseCode();                 
        if (response == HttpURLConnection.HTTP_OK) {
            in = httpConn.getInputStream();                                 
        }                     
    }
    catch (Exception ex)
    {
        throw new IOException("Error connecting");            
    }
    return in;     
}
	private void SavePreferences(String key, String value){
	    SharedPreferences sharedPreferences = getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
	    editor.putString(key, value);
	    editor.commit();
	    //Toast.makeText(getApplicationContext(),edtUser.getText().toString(), Toast.LENGTH_LONG).show();
		
	   }
	  
	   
} // end class
