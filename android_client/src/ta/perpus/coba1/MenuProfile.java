package ta.perpus.coba1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts.Data;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MenuProfile extends DashboardActivity {
	private TextView txtName,txtNim;
	private Bitmap PictBook;
	public String user,nama,vjudul,visbn,vkode,vtgl;
	private String url,url_ori,url2,url_ori2;

    private static String[] DATA = {""};
    private static String[] DATA2 = {""};

    private static final String APP_SHARED_PREFS = "ta.perpus.coba1.user";
	SharedPreferences sharedPreferences;
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_f2);
	        setTitleFromActivityLabel (R.id.title_text);
	        url_ori=getString(R.string.url_server)+"profile.php";
		    url=url_ori;
		    url_ori2=getString(R.string.url_server)+"loan.php";
		    url2=url_ori2;
	                
	        txtName = (TextView)findViewById(R.id.name);
	        txtNim = (TextView)findViewById(R.id.nim);
	        
	        sharedPreferences = getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);

	        String strSavedMem1 = sharedPreferences.getString("USER", "User tidak ada");
	        Toast.makeText(getApplicationContext(),strSavedMem1, Toast.LENGTH_LONG).show();
	        
	        url+="?user="+strSavedMem1;
	        url2+="?user="+strSavedMem1;
	        getMessagePHP(url);
	        getMessagePHP(url2);
	        txtName.setText("Name : "+ nama);
	        txtNim.setText("NIM       : "+strSavedMem1);
	        
	        ArrayList<SearchResults> searchResults = GetSearchResults();
	       
	        final ListView lv1 = (ListView) findViewById(R.id.ListView01);
	        lv1.setAdapter(new BaseAdapterProfile(this, searchResults));
	       
	        lv1.setOnItemClickListener(new OnItemClickListener() {
	        	@Override
				public void onItemClick(AdapterView<?> a, View v, int position,long id) {
					Object o = lv1.getItemAtPosition(position);
			          SearchResults fullObject = (SearchResults)o;
			          Bundle bundle = new Bundle();
			      	bundle.putString("param1", fullObject.getIsbn());

			      	Intent newIntent = new Intent(getApplicationContext(), F5Activity.class);
			      	newIntent.putExtras(bundle);
			      	startActivityForResult(newIntent, 0);
			          
			          //Toast.makeText(CommentActivity.this, "You have chosen: " + " " + fullObject.getName(), Toast.LENGTH_LONG).show();
			         
				} 
	        });
	    }
	   
	    private ArrayList<SearchResults> GetSearchResults(){
	     ArrayList<SearchResults> results = new ArrayList<SearchResults>();
	     
	     SearchResults sr1;
	     for (int i=0;i<=DATA.length-1;i++){
	    	 DATA2=DATA[i].split("~");
		     sr1= new SearchResults();
		     sr1.setTitle(DATA2[0]);
		     sr1.setBookCode(DATA2[1]);
		     sr1.setIsbn(DATA2[2]);
		     sr1.setLoanDate(DATA2[3]);
		     sr1.setLimitDate(DATA2[4]);
		     sr1.setLate(DATA2[5]+" day(s)");
		     PictBook=BitmapFactory.decodeResource(getResources(), R.drawable.bookblank);
		     sr1.setPictBook(PictBook);
		     results.add(sr1);
	     }
	     
	     
	     return results;
	    }

	    
		private String getMessagePHP(String alamat) 
		{
			int BUFFER_SIZE = 10;
			InputStream in = null;
			int charRead;
		    String str = "";
		    String hasil;
			
			try {
		        in = OpenHttpConnection(alamat);
		    } catch (IOException e1) {
		        // TODO Auto-generated catch block
		        e1.printStackTrace();
		        return "";
		    }
		    InputStreamReader isr = new InputStreamReader(in);
		    
		    char[] inputBuffer = new char[BUFFER_SIZE];
		    try {
		    	while ((charRead = isr.read(inputBuffer))>0) {
		    		//--convert chars to string
		    		String readString = String.copyValueOf(inputBuffer, 0, charRead);
		    		str += readString;
		    		inputBuffer = new char [BUFFER_SIZE];
		    	}
		    	hasil=str.toString();
		    	//Toast.makeText(getApplicationContext(),hasil, Toast.LENGTH_LONG).show();
		    	if (alamat.equals(url)){nama=hasil;}
		    	else if (alamat.equals(url2)){
		    		DATA=hasil.split("#");
		    	}
		    	in.close();
		    }
		    catch (IOException ex) {
		    	ex.printStackTrace();
		    	return "";
		    }
		    return str;
		}
		private InputStream OpenHttpConnection(String urlString) throws IOException
	{
	    InputStream in = null;
	    int response = -1;
	           
	    URL url = new URL(urlString); 
	    URLConnection conn = url.openConnection();
		//Toast.makeText(getBaseContext(), url.toString(), Toast.LENGTH_SHORT).show();
		  
	    if (!(conn instanceof HttpURLConnection))                     
	        throw new IOException("Not an HTTP connection");
	    
	    try{
	        HttpURLConnection httpConn = (HttpURLConnection) conn;
	        httpConn.setAllowUserInteraction(false);
	        httpConn.setInstanceFollowRedirects(true);
	        httpConn.setRequestMethod("GET");
	        httpConn.connect(); 

	        response = httpConn.getResponseCode();                 
	        if (response == HttpURLConnection.HTTP_OK) {
	            in = httpConn.getInputStream();                                 
	        }                     
	    }
	    catch (Exception ex)
	    {
	        throw new IOException("Error connecting");            
	    }
	    return in;     
	}
	}
