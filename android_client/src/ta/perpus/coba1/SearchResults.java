package ta.perpus.coba1;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class SearchResults {
	 private String name = "";
	 private String time = "";
	 private String title = "";
	 private String bookcode = "";
	 private String isbn = "";
	 private String loandate = "";
	 private String limitdate = "";
	 private String late = "";
	 private String pesan="";
	 private Bitmap pictbook;

	 public void setName(String name) {
	  this.name = name;
	 }
	 public String getName() {
	  return name;
	 }

	 public void setTime(String cityState) {
	  this.time = cityState;
	 }
	 public String getTime() {
	  return time;
	 }
	 
	 public void setTitle(String title) {
		 this.title = title;
	 }
	 public String getTitle() {
		 return title;
	 }

	 public void setBookCode(String BookCode) {
		 this.bookcode= BookCode;
	 }
	 public String getBookCode() {
		 return bookcode;
	 }
	
	 public void setIsbn(String isbn) {
		 this.isbn = isbn;
	 }
	 public String getIsbn() {
		 return isbn;
	 }

	 public void setLoanDate(String loandate) {
		 this.loandate = loandate;
	 }
	 public String getLoanDate() {
		 return loandate;
	 }
			 
	 public void setLimitDate(String limitdate) {
		 this.limitdate = limitdate;
	 }
	 public String getLimitDate() {
		 return limitdate;
	 }

	 public void setLate(String late) {
		 this.late = late;
	 }
	 public String getLate() {
		 return late;
	 }
	 public void setPesan(String pesan) {
		 this.pesan =pesan;
	 }
	 public String getPesan() {
		 return pesan;
	 }

	 public void setPictBook(Bitmap book) {
		 this.pictbook = book;
	 }
	 public Bitmap getPictBook() {
		 return pictbook;
	 }
	}