
package ta.perpus.coba1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegActivity extends DashboardActivity
{
	private String url,url_ori,isiNim,isiNama,isiAlamat,isiEmail,isiPass,isiRepass,isiNope;
    private Button btnLogin;
    private TextView btnReg;
    private EditText edtNim,edtPass,edtNope,edtAdds,edtEmail,edtRepass,edtNama;

protected void onCreate(Bundle savedInstanceState) 
{
    super.onCreate(savedInstanceState);
    setContentView (R.layout.activity_reg);
    setTitleFromActivityLabel (R.id.title_text);
    url_ori=getString(R.string.url_server)+"registrasi.php";
    url=url_ori;
    
    edtNim=(EditText)findViewById(R.id.etnim);
    isiNim=edtNim.getText().toString();
    edtNim.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			edtNim.setText("");
		}
	});
    
    edtNama=(EditText)findViewById(R.id.etname);
    isiNama=edtNama.getText().toString();
    edtNama.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			edtNama.setText("");
		}
	});
    
    edtAdds=(EditText)findViewById(R.id.etaddress);
    isiAlamat=edtAdds.getText().toString();
    edtAdds.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			edtAdds.setText("");
		}
	});
    
    edtNope=(EditText)findViewById(R.id.etnope);
    isiNope=edtNope.getText().toString();
    edtNope.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			edtNope.setText("");
		}
	});
    edtEmail=(EditText)findViewById(R.id.etemail);
    isiEmail=edtEmail.getText().toString();
    edtEmail.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			edtEmail.setText("");
		}
	});
    

    edtPass=(EditText)findViewById(R.id.etpass);
    isiPass=edtPass.getText().toString();
    edtPass.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			edtPass.setText("");
		}
	});
    edtRepass=(EditText)findViewById(R.id.etrepass);
    isiRepass=edtRepass.getText().toString();
    edtRepass.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			edtRepass.setText("");
		}
	});
    
    btnLogin =(Button)findViewById(R.id.login);
    btnLogin.setOnClickListener(new Button.OnClickListener(){
        public void onClick(View v){
        	if (isiPass==isiRepass){
        		
	        	url+="?user="+isiNim+"&name="+isiNama+"&addr="+isiAlamat+"&nope="+isiNope+"&email="+isiEmail+"&pass="+isiPass;
	        	//Toast.makeText(getApplicationContext(),url.toString(), Toast.LENGTH_LONG).show();
		        getMessagePHP(url);
		        url=url_ori;
        	}	else {
        		Toast.makeText(getApplicationContext(),"Isi Password dengan benar!", Toast.LENGTH_LONG).show();
        	}
        }
    });
    btnReg=(TextView)findViewById(R.id.reg);
    btnReg.setText(Html.fromHtml("<u>User Verification</u> "));
    btnReg.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			startActivity (new Intent(getApplicationContext(), VerificationActivity.class));
			
		}
	});
}

private String getMyPhoneNumber(){
    TelephonyManager mTelephonyMgr;
    mTelephonyMgr = (TelephonyManager)
            getSystemService(Context.TELEPHONY_SERVICE); 
    return mTelephonyMgr.getLine1Number();
}

private String getMy10DigitPhoneNumber(){
    String s = getMyPhoneNumber();
    return s.substring(2);
}
private String getMessagePHP(String alamat) 
{
	int BUFFER_SIZE = 10;
	InputStream in = null;
	int charRead;
    String str = "";
    String hasil;
	
	try {
        in = OpenHttpConnection(alamat);
    } catch (IOException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
        return "";
    }
    InputStreamReader isr = new InputStreamReader(in);
    
    char[] inputBuffer = new char[BUFFER_SIZE];
    try {
    	while ((charRead = isr.read(inputBuffer))>0) {
    		//--convert chars to string
    		String readString = String.copyValueOf(inputBuffer, 0, charRead);
    		str += readString;
    		inputBuffer = new char [BUFFER_SIZE];
    	}
    	hasil=str.toString();
    	//Toast.makeText(getApplicationContext(),hasil, Toast.LENGTH_LONG).show();
    	if (hasil.equals("1")){
        	startActivity (new Intent(getApplicationContext(), VerificationActivity.class));
        	Toast.makeText(getApplicationContext(),"Registrasi Berhasil!. Silahkan tunggu SMS verifikasi", Toast.LENGTH_LONG).show();}
    	else {Toast.makeText(getApplicationContext(),"Registrasi gagal!", Toast.LENGTH_LONG).show();
		}
    	in.close();
    }
    catch (IOException ex) {
    	ex.printStackTrace();
    	return "";
    }
    return str;
}
private InputStream OpenHttpConnection(String urlString) throws IOException
{
    InputStream in = null;
    int response = -1;
           
    URL url = new URL(urlString); 
    URLConnection conn = url.openConnection();
	//Toast.makeText(getBaseContext(), url.toString(), Toast.LENGTH_SHORT).show();
	  
    if (!(conn instanceof HttpURLConnection))                     
        throw new IOException("Not an HTTP connection");
    
    try{
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false);
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setRequestMethod("GET");
        httpConn.connect(); 

        response = httpConn.getResponseCode();                 
        if (response == HttpURLConnection.HTTP_OK) {
            in = httpConn.getInputStream();                                 
        }                     
    }
    catch (Exception ex)
    {
        throw new IOException("Error connecting");            
    }
    return in;     
}
    
} // end class
