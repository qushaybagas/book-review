
package ta.perpus.coba1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MenuAuthor extends ListActivity implements OnClickListener{
	
	private String url,url2,url3;
    
    private String konten;
    
    private static class EfficientAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public EfficientAdapter(Context context) {
            // Cache the LayoutInflate to avoid asking for a new one each time.
            mInflater = LayoutInflater.from(context);

        }

        public int getCount() {
            return DATA.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            // When convertView is not null, we can reuse it directly, there is no need
            // to reinflate it. We only inflate a new View when the convertView supplied
            // by ListView is null.
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item_author, null);

                // Creates a ViewHolder and store references to the two children views
                // we want to bind data to.
                holder = new ViewHolder();
                holder.Author = (TextView) convertView.findViewById(R.id.author);

                convertView.setTag(holder);
            } else {
                // Get the ViewHolder back to get fast access to the TextView
                // and the ImageView.
                holder = (ViewHolder) convertView.getTag();
            }

            // Bind the data efficiently with the holder.
            holder.Author.setText(DATA[position]);

            return convertView;
        }

        static class ViewHolder {
            TextView Author;
        }
    }

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new EfficientAdapter(this));
        ListView list = getListView();
		list.setTextFilterEnabled(true);
		url = getString(R.string.url_server)+"list_author.php";
	    url2 = getString(R.string.url_server)+"list_author_buku.php?author=";
	    url3 = getString(R.string.url_server)+"list_author_isbn.php?author=";
		getMessagePHP(url);
		
		list.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				konten = (String) ((TextView) arg1).getText();
				String konten2=konten.toString().replace(" ", "%20");
				url2 += konten2;
				url3 += konten2;
				getMessagePHP(url2);
				getMessagePHP(url3);
					onClick(arg1);
				
//					url2 = "http://10.0.2.2/perpus/list_author_buku.php?author=";
//					url3 = "http://10.0.2.2/perpus/list_author_isbn.php?author=";
				    url2 = getString(R.string.url_server)+"list_author_buku.php?author=";
				    url3 = getString(R.string.url_server)+"list_author_isbn.php?author=";
				
				
				}
			});	
    }

    private static String[] DATA = {""};
    private static String[] DATA2 = {""};
    private static String[] DATA3 = {""};
	@Override
	public void onClick(View v) {
		
		AlertDialog.Builder alertbox = new AlertDialog.Builder(MenuAuthor.this);
		alertbox.setTitle("Book List");
		
        alertbox.setItems(DATA2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	String isi = DATA3[which];
            	Bundle bundle = new Bundle();
            	bundle.putString("param1", isi);

            	Intent newIntent = new Intent(getApplicationContext(), F5Activity.class);
            	newIntent.putExtras(bundle);
            	startActivityForResult(newIntent, 0);
				
            }
        });

        alertbox.show();
	}
	private String getMessagePHP(String alamat) 
    {
    	int BUFFER_SIZE = 10;
    	InputStream in = null;
    	int charRead;
        String str = "";
        String hasil;
    	
    	try {
            in = OpenHttpConnection(alamat);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "";
        }
        InputStreamReader isr = new InputStreamReader(in);
        
        char[] inputBuffer = new char[BUFFER_SIZE];
        try {
        	while ((charRead = isr.read(inputBuffer))>0) {
        		//--convert chars to string
        		String readString = String.copyValueOf(inputBuffer, 0, charRead);
        		str += readString;
        		inputBuffer = new char [BUFFER_SIZE];
        	}
        	hasil=str.toString();
        	if (alamat.equals(url)){DATA=hasil.split("~");}
        	else if (alamat.equals(url2)){DATA2=hasil.split("~");}
        	else if (alamat.equals(url3)){DATA3=hasil.split("~");}
        	
        	in.close();
        }
        catch (IOException ex) {
        	ex.printStackTrace();
        	return "";
        }
        return str;
    }
	private InputStream OpenHttpConnection(String urlString) throws IOException
    {
        InputStream in = null;
        int response = -1;
               
        URL url = new URL(urlString); 
        URLConnection conn = url.openConnection();
    	//Toast.makeText(getBaseContext(), url.toString(), Toast.LENGTH_SHORT).show();
    	  
        if (!(conn instanceof HttpURLConnection))                     
            throw new IOException("Not an HTTP connection");
        
        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect(); 

            response = httpConn.getResponseCode();                 
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();                                 
            }                     
        }
        catch (Exception ex)
        {
            throw new IOException("Error connecting");            
        }
        return in;     
    }
}
