
package ta.perpus.coba1;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public abstract class DashboardActivity extends Activity 
{

	protected void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	}

	protected void onDestroy ()
	{
	   super.onDestroy ();
	}

	protected void onPause ()
	{
	   super.onPause ();
	}

	
	protected void onRestart ()
	{
	   super.onRestart ();
	}


	protected void onResume ()
	{
	   super.onResume ();
	}

	protected void onStart ()
	{
	   super.onStart ();
	}

	
	protected void onStop ()
	{
	   super.onStop ();
	}

	public void onClickHome (View v)
	{
	    goHome (this);
	}

	public void onClickSearch (View v)
	{
	    startActivity (new Intent(getApplicationContext(), SearchActivity.class));
	}

	public void onClickAbout (View v)
	{
	    startActivity (new Intent(getApplicationContext(), AboutActivity.class));
	}
	
	public void onClickFeature (View v)
	{
	    int id = v.getId ();
	    switch (id) {
	      case R.id.home_btn_feature1 :
	    	
	    	  Intent intent = new Intent("com.google.zxing.client.android.SCAN");
	          startActivityForResult(intent, 0);
               break;
	      case R.id.home_btn_feature2 :
	           startActivity (new Intent(getApplicationContext(), MenuProfile.class));
	           break;
	      case R.id.home_btn_feature3 :
	           startActivity (new Intent(getApplicationContext(), MenuBookList.class));
	           break;
	      case R.id.home_btn_feature4 :
	           startActivity (new Intent(getApplicationContext(), MenuAuthor.class));
	           break;
	      default: 
	    	   break;
	    }
	}


	public void goHome(Context context) 
	{
	    final Intent intent = new Intent(context, PerpusActivity.class);
	    intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    context.startActivity (intent);
	}

	public void setTitleFromActivityLabel (int textViewId)
	{
	    TextView tv = (TextView) findViewById (textViewId);
	    if (tv != null) tv.setText (getTitle ());
	} // end setTitleText


public void toast (String msg)
{
    Toast.makeText (getApplicationContext(), msg, Toast.LENGTH_SHORT).show ();
} // end toast

/**
 * Send a message to the debug log and display it using Toast.
 */
public void trace (String msg) 
{
    Log.d("Demo", msg);
    toast (msg);
}

public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    if (requestCode == 0) {
        if (resultCode == RESULT_OK) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
            // Handle successful scan
            Toast toast = Toast.makeText(this, "ISBN:" + contents, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 25, 400);
            toast.show();
            
            Bundle bundle = new Bundle();
        	bundle.putString("param1", contents);
            Intent newIntent = new Intent(getApplicationContext(), F5Activity.class);
        	newIntent.putExtras(bundle);
        	startActivityForResult(newIntent, 0);
        } 
    }
}


} // end class
